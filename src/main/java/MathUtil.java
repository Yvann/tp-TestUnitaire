import java.util.List;

/**
 * Created by edenayrolles on 16/10/2015.
 */
public class MathUtil {

    public static Integer moyenne(List<Integer> liste) {

        if (liste == null) {
            return null;
        }

        Integer result = null;
        if (liste.size() == 0)
            result = 0;
        else {
            Integer total = 0;
            for (Integer i : liste)  {
                total += i;
            }
            result = total / liste.size();
        }
        return result;
    }

}
