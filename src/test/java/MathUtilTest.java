import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;


/**
 * Created by edenayrolles on 16/10/2015.
 */
public class MathUtilTest {

    private List integers;

    @Before
    public void init() {
        this.integers = new ArrayList<Integer>();
    }

    @Test
    public void testMoyenneListeVide() {
        assertEquals(new Integer(0), MathUtil.moyenne(integers));
    }

    @Test
    public void testMoyenneListeNull() {
        assertNull(MathUtil.moyenne(null));
    }

    @Test
    public void testMoyenne() {

        assertEquals(new Integer(0), MathUtil.moyenne(integers));

        integers.add(2);

        assertEquals(new Integer(2), MathUtil.moyenne(integers));

        integers.add(8);
        integers.add(16);

        assertEquals(new Integer(8), MathUtil.moyenne(integers));
    }

    @Test
    public void testMoyenneError() {

        assertEquals(new Integer(0), MathUtil.moyenne(integers));

        integers.add(2);

        assertEquals(new Integer(2), MathUtil.moyenne(integers));

        integers.add(8);
        integers.add(100);
        assertNotEquals(new Integer(8), MathUtil.moyenne(integers));

    }
}
